package com.example.recycler;


/**
 *设置今天器
 */
public interface OnItemClickListener {

  void onItemClick(Character character);

  void onLongItemClick(Character character);
}
