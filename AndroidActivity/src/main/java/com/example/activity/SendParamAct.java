package com.example.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class SendParamAct extends AppCompatActivity {

    public static final String K_INT = "k_int";
    public static final String K_BOOL = "k_bool";
    public static final String K_STR = "k_str";
    public static final String K_PARCEL = "k_parcel";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_param);
        Intent intent = getIntent();
        if (intent !=null){
            String stringExtra = intent.getStringExtra(K_STR);
            int intExtra = intent.getIntExtra(K_INT,0);
            TextView textView = (TextView) findViewById(R.id.p_v1);
            textView.setText(stringExtra+"=========="+intExtra);
        }
    }
}