package com.example.notification;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {


    private NotificationManager manager;
    private Notification notification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {   //版本大于等于 安卓8.0
            NotificationChannel channel = new NotificationChannel("leo", "测试通知", NotificationManager.IMPORTANCE_HIGH);
            manager.createNotificationChannel(channel);
        }
        notification = new NotificationCompat.Builder(this, "leo")
                .setContentTitle("重大消息!!!")    //设置标题
                .setContentText("世界那么大，你想去看看嘛")    //设置通知文字
                .setSmallIcon(R.mipmap.ic_launcher)   //设置左边的小图标
                .build();
    }

    //开启通知按钮  点击事件
    public void setNotification(View view) {
        manager.notify(1, notification);    //点击事件启动唤醒manager，用于通知notification
    }

    //关闭通知按钮   点击事件
    public void cacelNotification(View view) {

    }
}