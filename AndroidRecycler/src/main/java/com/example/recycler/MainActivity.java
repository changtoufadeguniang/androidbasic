package com.example.recycler;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.os.ResultReceiver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


/**
 * RecyclerView主要实现列表效果
 * 实现步骤：
 * 1.写ViewHolder类和item布局 文件
 * 2.设计Adapter适配器实现三个方法onCreateViewHolder onBindViewHolder getItemCount
 * 3.设置RecyclerView 适配器与LayoutManager设置两个属性
 */
public class MainActivity extends AppCompatActivity {

    LetterAdapter mLetterAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        List<Character> characterList = new ArrayList<>();
        for (char c = 'a'; c <= 'z'; c++) {
            characterList.add(c);
        }
        mLetterAdapter  = new LetterAdapter(characterList);
        //适配器用了监听功能
        mLetterAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(Character character) {
                Toast.makeText(getApplicationContext(), "Click " + character, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongItemClick(Character character) {
                Toast.makeText(getApplicationContext(), "Long Click " + character, Toast.LENGTH_SHORT).show();
            }
        });
        RecyclerView recyclerView = findViewById(R.id.re_view);
        recyclerView.setAdapter(mLetterAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));
    }

 


    private class LetterAdapter extends RecyclerView.Adapter<VH>{

        private List<Character>  dataList;

        private OnItemClickListener onItemClickListener;

        public LetterAdapter(List<Character> dataList) {
            this.dataList = dataList;
        }

        public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
            this.onItemClickListener = onItemClickListener;
        }

        @NonNull
        @Override
        public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new VH(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_letter,parent,false));
        }

        @Override
        public void onBindViewHolder(@NonNull VH holder, int position) {
            Character c = dataList.get(position);
            holder.tv1.setText(c.toString());
            holder.tv2.setText(String.valueOf(Integer.valueOf(c)));
            holder.item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(onItemClickListener!=null){
                        onItemClickListener.onItemClick(c);
                    }
                }
            });
            holder.item.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if(onItemClickListener !=null){
                        onItemClickListener.onLongItemClick(c);
                    }
                    return true;
                }
            });
        }

        @Override
        public int getItemCount() {
            return dataList.size();
        }


    }


    private class VH extends RecyclerView.ViewHolder {

        View item;

        TextView tv1;

        TextView tv2;

        public VH(@NonNull View itemView) {
            super(itemView);
            item = itemView;
            tv1 = itemView.findViewById(R.id.tv1);
            tv2 = itemView.findViewById(R.id.tv2);
        }


    }
}