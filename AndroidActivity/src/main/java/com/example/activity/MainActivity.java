package com.example.activity;

import android.content.Intent;
import android.os.Bundle;

import com.rustfisher.baselib.AbsGuideAct;
import com.rustfisher.baselib.GuideAdapter;

import java.util.Arrays;

public class MainActivity extends AbsGuideAct {

    private static final int K_ACT_LIFE = 1;

    private static final int K_ACT_SEND_PARAM = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mGuideAdapter.setDataList(Arrays.asList(
                new GuideAdapter.OptionItem(K_ACT_LIFE, "Activity生命周期"),
                new GuideAdapter.OptionItem(K_ACT_SEND_PARAM, "Activity跳转传递参数")
               /* new GuideAdapter.OptionItem(K_ACT_SEND_PARAM, "Activity传递参数"),
                new GuideAdapter.OptionItem(K_ACT_RES, "Activity返回时传递参数"),
                new GuideAdapter.OptionItem(K_ACT_RES_REG, "Activity返回时传递参数 - LAUNCHER"),
                new GuideAdapter.OptionItem("悬浮Activity 可拖动", "缩小后可以拖动，可还原大小，外部可点击", true, FloatingScaleAct.class)*/
        ));
        mGuideAdapter.setOnItemClickListener(item -> {
            switch (item.num) {
                case K_ACT_LIFE:
                    startActivity(new Intent(getApplicationContext(), LifeCycleAct.class));
                    break;
                case K_ACT_SEND_PARAM:
                    Intent intent = new Intent(getApplicationContext(), SendParamAct.class);
                    intent.putExtra(SendParamAct.K_INT, 100);
                    intent.putExtra(SendParamAct.K_STR, "传递的字符串");
                    startActivity(intent);
                    break;
            }
        });
    }

}